//
//  sideView.m
//  side
//
//  Created by Click Labs130 on 11/18/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import "sideView.h"

@implementation sideView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//NSNotification    protocols



NSArray *color;
-(void) startSideView{

    UITableView * tableView;
    tableView=[[UITableView alloc]init];
    tableView.frame=CGRectMake(0, 5, 260, 550);
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    tableView.delegate=self;
    tableView.dataSource = self;
    [self addSubview:tableView];
    
    color= @[@"red",@"green", @"yellow"];
    [tableView reloadData];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return color.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier   forIndexPath:indexPath] ;
    cell.textLabel.text = color[indexPath.row];

    return  cell;
}

@end


