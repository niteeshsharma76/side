//
//  ViewController.m
//  side
//
//  Created by Click Labs130 on 11/18/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *menu;

@end

@implementation ViewController
@synthesize menu;
NSString *name;
NSString *class;
NSString *branch;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    SampleProtocol *sampleProtocol = [[SampleProtocol alloc]init];
    sampleProtocol.delegate = self;
    self.view.backgroundColor=[UIColor redColor];
    [sampleProtocol startSampleProcess];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Sample protocol delegate
-(void)processCompleted{
    //[myLabel setText:@"Process Completed"];
}
@end
