//
//  sideView.h
//  side
//
//  Created by Click Labs130 on 11/18/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
// Protocol definition starts here
@protocol SampleProtocolDelegate <NSObject>
@required
- (void) processCompleted;
@end
// Protocol Definition ends here
@interface SampleProtocol : NSObject

{
    // Delegate to respond back
    id <SampleProtocolDelegate> _delegate;
    
}
@property (nonatomic,strong) id delegate;

-(void)startSampleProcess; // Instance method

@end
@interface sideView : UIView<UITableViewDataSource,UITableViewDelegate>


@end
